package com.example.flickr_app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Half;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button get_image_button = (Button) findViewById(R.id.get_image_button);
        get_image_button.setOnClickListener(new GetImageOnClickListener());

        Button get_list_button = (Button) findViewById(R.id.get_list_button);
        get_list_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                startActivity(intent);
            }
        });

    }


    private class GetImageOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            new AsyncFlickrJSONData().execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json");
        }
    }

    public class AsyncFlickrJSONData extends AsyncTask<String, Void, JSONObject> {
        @Override
        protected void onPreExecute() {
            Log.i("started", "OK 1");
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            Log.i("before_url_recup", "OK 2");
            URL url = null;
            String url_string_input = strings[0];
            Log.i("after_url_recup", "OK 3");

            JSONObject output = new JSONObject();
            Log.i("after_url_recup", "OK 4");
            try {

                url = new URL(url_string_input);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    Log.i("created_input_stream", "OK 5");
                    String s = readStream(in);
                    String readyForJSON = s.substring(15, s.length()-1);

                    /*Log.i("result_read_stream", readyForJSON);*/
                    output = new JSONObject(readyForJSON);


                } catch (JSONException e) {
                    throw new RuntimeException(e);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return output;
        }

        @Override
        protected void onPostExecute(JSONObject object) {
            String image_url_string="";
            String s = object.toString();
            Log.i("JSON_result", s);

            JSONObject image = null;
            try {
                image = object.getJSONArray("items").getJSONObject(0);
                image_url_string = image.getJSONObject("media").getString("m");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            new AsyncBitmapDownloader().execute(image_url_string);

        }
    }

    public class AsyncBitmapDownloader extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... strings) {
            String url_string = strings[0];
            Bitmap bm = null;

            URL url = null;
            try {
                url = new URL(url_string);

                try {
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    bm = BitmapFactory.decodeStream(in);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return bm;
        }

        @Override
        protected void onPostExecute(Bitmap bm) {
            ImageView image_view = (ImageView) findViewById(R.id.image);

            image_view.setImageBitmap(bm);
        }
    }


    public String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }
}