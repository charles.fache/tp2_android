package com.example.flickr_app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        MyAdapter adapter = new MyAdapter();

        new AsyncFlickrJSONDataForList(adapter).execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json");
    }

    private class MyAdapter extends BaseAdapter {
        Vector<String> vector = new Vector<String>();

        public void dd(String url) {
            vector.add(url);
        }

        @Override
        public int getCount() {
            return vector.size();
        }

        @Override
        public Object getItem(int position) {
            return vector.get(position);
        }

        @Override
        public long getItemId(int position) {
            Log.i("get_item_id", "TODO");
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Log.i("get_view", "TODO");
            return null;
        }
    }

    public class AsyncFlickrJSONDataForList extends AsyncTask<String, Void, JSONObject> {
        MyAdapter adapter;
        public AsyncFlickrJSONDataForList(MyAdapter adapter) {
            this.adapter = adapter;
        }
        @Override
        protected void onPreExecute() {
            Log.i("started", "OK 1");
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            Log.i("before_url_recup", "OK 2");
            URL url = null;
            String url_string_input = strings[0];
            Log.i("after_url_recup", "OK 3");

            JSONObject output = new JSONObject();
            Log.i("after_url_recup", "OK 4");
            try {

                url = new URL(url_string_input);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    Log.i("created_input_stream", "OK 5");
                    String s = readStream(in);
                    String readyForJSON = s.substring(15, s.length()-1);

                    output = new JSONObject(readyForJSON);


                } catch (JSONException e) {
                    throw new RuntimeException(e);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return output;
        }

        @Override
        protected void onPostExecute(JSONObject object) {
            String image_url_string="";
            /*String s = object.toString();
            Log.i("JSON_result", s);*/

            JSONObject image = null;

            for (int i = 0; i<20; i++) {
                try {
                    image = object.getJSONArray("items").getJSONObject(i);
                    image_url_string = image.getJSONObject("media").getString("m");
                    adapter.dd(image_url_string);
                    Log.i("JFL", "Adding to adapter url : " + image_url_string);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            adapter.notifyDataSetChanged();
            /*try {
                image = object.getJSONArray("items").getJSONObject(0);
                image_url_string = image.getJSONObject("media").getString("m");
            } catch (JSONDException e) {
                e.printStackTrace();
            }*/

            /*new MainActivity.AsyncBitmapDownloader().execute(image_url_string);*/

        }
    }

    public String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }
}