package com.example.tp2_android;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button authenticate_button = (Button)findViewById(R.id.authenticate_button);
        EditText user_login = (EditText) findViewById(R.id.user_login);
        EditText user_password = (EditText) findViewById(R.id.user_password);
        TextView auth_result = (TextView) findViewById(R.id.auth_result);


        authenticate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String login = user_login.getText().toString();
                        String password = user_password.getText().toString();

                        URL url = null;

                        JSONObject output = new JSONObject();
                        try {
                            String formatted_credentials = login + ":" + password;

                            url = new URL("https://httpbin.org/basic-auth/bob/sympa");
                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                            String basicAuth = "Basic " + Base64.encodeToString(formatted_credentials.getBytes(), Base64.NO_WRAP);
                            urlConnection.setRequestProperty ("Authorization", basicAuth);

                            try {
                                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                                String s = readStream(in);
                                Log.i("result_read_stream", s);
                                output = new JSONObject(s);


                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            } finally {
                                urlConnection.disconnect();
                            }
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        runOnUiThread(new updateUI(output, auth_result));

                    }
                }).start();
            }
        });


    }

    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }

    private static class updateUI implements Runnable {
        JSONObject data;
        TextView result_view;

        public updateUI(JSONObject json_data, TextView view) {
            this.data = json_data;
            this.result_view = view;
        }

        @Override
        public void run() {
            try {
                Boolean authentication_res = data.getBoolean("authenticated");
                if (authentication_res) {
                    result_view.setText("authentication succeeded");
                } else {
                    result_view.setText("authentication failed :("); //pretty sure this never happens, as website only sends json if auth succeeded.
                }
            } catch (JSONException e) {
                Log.i("auth_result", e.toString());
                result_view.setText("authentication failed :(");
            }
        }
    }
}